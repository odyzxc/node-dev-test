import { ASort } from "./ASort";
import { BSort } from "./BSort";
import { BSearch } from "./BSearch";

const unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32];
const elementsToFind = [1, 5, 13, 27, 77];

const arraysEqual = (input1: number[], input2: number[]): boolean => {
  if (!Array.isArray(input1) || !Array.isArray(input2)) {
    return false;
  }
  if (input1.length !== input2.length) {
    return false;
  }
  return input1.reduce((result: boolean, element: number, index: number) => {
    if (element !== input2[index]) {
      return false;
    }
    return result;
  }, true);
};

const sortedArray = [ 0, 2, 4, 5, 7, 9, 13, 14, 17, 22, 32, 65, 77, 83 ];

console.log("Test array ", arraysEqual(sortedArray, ASort.sort(unsorted)));

elementsToFind.forEach(elem => {
  const foundIndex = BSearch.instance().search(sortedArray, elem);
  const foundIndexTest = sortedArray.indexOf(elem);
  console.log(`Searched element ${elem} found index ${foundIndex}`);
  console.log('FoundIndex correct: ', foundIndex === foundIndexTest);
});

console.log('Number of search operations performed: ', BSearch.instance().operations);
