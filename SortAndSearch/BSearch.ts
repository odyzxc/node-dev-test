export class BSearch {
  private static _instance: BSearch;
  private static _operations = 0;
  private constructor() {}

  private static _search(array: number[], element: number, offset = 0): number {
    if (array.length === 0) {
      return -1;
    }
    if (array.length === 1) {
      return array[0] === element ? 0 + offset : -1;
    }
    const center = Math.floor(array.length / 2);
    if (array[center] === element) {
      return center + offset;
    }

    return element < array[center]
      ? BSearch._search(array.slice(0, center), element, offset)
      : BSearch._search(array.slice(center + 1, array.length), element, offset + center + 1);
  }

  static instance() {
    if (!BSearch._instance) {
      BSearch._instance = new BSearch();
    }
    return BSearch._instance;
  }

  search(array: number[], element: number): number {
    BSearch._operations++;
    return BSearch._search(array, element);
  }

  get operations() {
    return BSearch._operations;
  }
}
