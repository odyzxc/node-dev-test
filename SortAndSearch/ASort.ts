// merge sort
export class ASort {
  static sort(input: number[]): number[] {
    if (!input || input.length <= 1) {
      return input;
    }
    const center = Math.floor(input.length / 2);
    const left = input.slice(0, center);
    const right = input.slice(center);

    return ASort.merge(ASort.sort(left), ASort.sort(right));
  }
  private static merge(left: number[], right: number[]): number[] {
    const array = [];
    let leftIndex = 0;
    let rightIndex = 0;
    const numberOfElements = left.length + right.length;

    while (leftIndex + rightIndex < numberOfElements) {
      const leftItem = left[leftIndex];
      const rightItem = right[rightIndex];

      if (leftItem === undefined) {
        array.push(rightItem);
        rightIndex++;
        continue;
      }
      if (rightItem === undefined) {
        array.push(leftItem);
        leftIndex++;
        continue;
      }
      if (leftItem < rightItem) {
        array.push(leftItem);
        leftIndex++;
        continue;
      }
      array.push(rightItem);
      rightIndex++;
    }
    return array;
  }
}
