import { Injectable, Inject } from '@nestjs/common';
import { plainToClass } from 'class-transformer';

import { Repository } from './Repository';
import { Order } from '../Model/Order';
import { Product } from '../Model/Product';
import { Customer } from '../Model/Customer';
import { IBestSellers, IBestBuyers } from '../../Report/Model/IReports';

interface IProductComparison {
  [id: number]: {
    product: Product;
    total: number;
  };
}

interface ICustomerComparison {
  [id: number]: {
    customer: Customer;
    total: number;
  };
}

@Injectable()
export class OrderMapper {
  @Inject() repository: Repository;

  private async getMappedOrders(): Promise<Order[]> {
    const orders = await this.repository.fetchOrders();
    const products = await this.repository.fetchProducts();
    const customers = await this.repository.fetchCustomers();

    const mappedOrders = orders.map(o => ({
      ...o,
      customer: customers.find(c => c.id === o.customer),
      products: o.products.map(p => products.find(product => product.id === p)),
    }));

    return plainToClass(Order, mappedOrders);
  }

  private async getOrdersFromDate(date: string): Promise<Order[]> {
    const orders = await this.getMappedOrders();
    return orders.filter(o => o.createdAt === date);
  }

  private getBestSellingProductFromComparison(
    comparison: IProductComparison,
  ): IBestSellers {
    const bestProduct = Object.keys(comparison).reduce(
      (bestseller, productId) => {
        if (!bestseller.product) {
          return comparison[productId];
        }
        if (comparison[productId].total > bestseller.total) {
          return comparison[productId];
        }
        return bestseller;
      },
      { total: 0, product: undefined },
    );
    return {
      productName: bestProduct.product.name,
      quantity: bestProduct.quantity,
      totalPrice: bestProduct.total,
    };
  }

  private getBestBuyerFromCOmparison(comparison: ICustomerComparison): IBestBuyers {
    const bestBuyer = Object.keys(comparison).reduce(
      (bestBuyer, customerId) => {
        if (!bestBuyer.customer) {
          return comparison[customerId];
        }
        if (comparison[customerId].total > bestBuyer.total) {
          return comparison[customerId];
        }
        return bestBuyer;
      },
      { total: 0, customer: undefined },
    );
    return {
      customerName: bestBuyer.customer.fullName,
      totalPrice: bestBuyer.total
    }
  }

  // in real world application I would use other tools for reports - like mongodb aggregations or SQL indexed/materialized views
  async getBestSellingProduct(date: string): Promise<IBestSellers> {
    const ordersFromDate = await this.getOrdersFromDate(date);
    const productsSales = ordersFromDate.reduce((productComparison, order) => {
      order.products.forEach(product => {
        if (productComparison[product.id]) {
          productComparison[product.id] = {
            ...productComparison[product.id],
            total: productComparison[product.id].total + product.price,
            quantity: productComparison[product.id].quantity + 1,
          };
          return productComparison;
        }
        productComparison[product.id] = {
          product: product,
          total: product.price,
          quantity: 1,
        };
      });
      return productComparison;
    }, {});
    return this.getBestSellingProductFromComparison(productsSales);
  }

  async getBestCustomer(date: string): Promise<IBestBuyers> {
    const ordersFromDate = await this.getOrdersFromDate(date);
    const ordersByCustomer = ordersFromDate.reduce((buyerComparison, order) => {
      if (buyerComparison[order.customer.id]) {
        buyerComparison[order.customer.id] = {
          ...buyerComparison[order.customer.id],
          total: buyerComparison[order.customer.id].total + order.totalPrice,
        };
        return buyerComparison;
      }
      buyerComparison[order.customer.id] = {
        total: order.totalPrice,
        customer: order.customer,
      };
      return buyerComparison;
    }, {});
    return this.getBestBuyerFromCOmparison(ordersByCustomer)
  }
}
