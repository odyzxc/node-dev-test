import { Type } from 'class-transformer';

import { Customer } from './Customer';
import { Product } from './Product';

export class Order {
  number: string;

  @Type(() => Customer)
  customer: Customer;

  createdAt: string;

  @Type(() => Product)
  products: Product[];

  get totalPrice() {
    return this.products.reduce((total, product) => total + product.price, 0)
  }
}
