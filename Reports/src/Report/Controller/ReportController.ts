import { Controller, Get, Param, Inject } from '@nestjs/common';
import { OrderMapper } from '../../Order/Service/OrderMapper';
import { IBestSellers } from '../Model/IReports';

@Controller()
export class ReportController {
  @Inject() orderMapper: OrderMapper;

  @Get('/report/products/:date')
  async bestSellers(@Param('date') date: string) {
    const bestSellingProductSummary = await this.orderMapper.getBestSellingProduct(
      date,
    );
    return bestSellingProductSummary;
  }

  @Get('/report/customer/:date')
  async bestBuyers(@Param('date') date: string) {
   const bestBuyer = await this.orderMapper.getBestCustomer(date);
   return bestBuyer;
  }
}
